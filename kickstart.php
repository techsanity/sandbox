<?php
include 'includes/bootstrap.php';
init();

$db = Registry::get('db');
?>
<style>
table {
	border-collapse: collapse;
}
thead {
	background-color: black;
	color: white;
}
td {
	border: 1px solid black;
	padding: 3px 6px;
}
h3 {
	margin-bottom: 0;
}
</style>
<form method="post" action="">
	<input type="submit" name="delete" value="Drop Database" />
	<input type="submit" value="Refresh Page" />
</form>
<?php
if (!empty($_POST['delete'])) {
    $db->drop();
	echo "<strong>Database reset</strong><br />";
	die();
}
?>
<?= $db->showAll('users') ?>
</table>
