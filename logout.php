<?php
include 'includes/bootstrap.php';
init();

$session = Registry::get('session');

$session->expire();

get_header(); ?>
<main>
	<h1>Goodbye</h1>
You have successfully logged out.
</main>
<?php get_footer(); ?>
