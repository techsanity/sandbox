<?php
include 'includes/bootstrap.php';
init();

function signup($db) {
	$errors = [];

	$real_name = $_POST['real_name'];
	if (empty($real_name)) {
		$errors[] = 'You must give your real name';
	} elseif (strstr($real_name, '--') !== false) {
		$errors[] = 'That doesn\'t look like a real name.';
	} elseif (preg_match('/[0-9]/', $real_name)) {
		$errors[] = 'That doesn\'t look like a real name.';
	} elseif (!strstr($real_name, ' ')) {
		$errors[] = 'That doesn\'t look like a <strong>full</strong> real name.';
	}

	$username = $_POST['username'];
	if (empty($username)) {
		$errors[] = 'You must choose a username';
	} elseif (preg_match('/[^a-zA-Z0-9_]/', $username)) {
		$errors[] = 'A username can only contain letters, numbers, and underscores';
	}

	$password = $_POST['password'];
	if (empty($password)) {
		$errors[] = 'You must set a password';
	} elseif (strlen($password) > 16) {
		$errors[] = 'Your password must not exceed 16 characters';
	} elseif (preg_match('/[^a-zA-Z0-9_ \&\!\.,:;]/', $password)) {
		$errors[] = 'A password can only contain letters, numbers, spaces, or any of these symbols: & ! . , : ;';
	}

	$gender = $_POST['gender'];
	if (empty($gender)) {
		$errors[] = 'You must designate male or female.';
	}

	if (count($errors) == 0) {
		$rs = $db->query("SELECT COUNT(*) FROM users WHERE username = '$username'");
		$count = $rs->fetch();
		if ($count[0] > 0) {
			$errors[] = 'That username is already taken.';
		}
	}

	if (count($errors)) {
		$signup_errors = $errors;
		include 'theme/homepage.php.htm';
	} else {
		$sql = "INSERT INTO users (username, name, password, gender, is_admin) ".
		"VALUES('$username', '$real_name', '$password', '$gender', 0)";

		$db->exec($sql);

		$sql = "SELECT user_id, is_admin FROM users WHERE username = '$username'";
		$rs = $db->query( $sql );

		$result = $rs->fetch();
		if ( $result != false ) {
			$session = Registry::get('session');
			$session->create( $result['user_id'], $result['is_admin'] );
		}

		include 'theme/first-use.php.htm';
	}
}

function login($db) {
	$errors = [];
	$username = @$_POST['username'];
	$password = @$_POST['password'];

	if (empty($username)) {
		$errors[] = 'You forgot to enter a username';
	}
	if (empty($password)) {
		$errors[] = 'You forgot to enter a password';
	}

	if (count($errors)) {
		$login_errors = $errors;
		include 'theme/homepage.php.htm';
	} else {
		$sql = "SELECT user_id, is_admin FROM users WHERE username = '$username' AND password = '$password'";
		$db->restrictMulti(true);
		$rs = $db->query( $sql );
		
		$result = $rs->fetch();
		if ( $result != false ) {
			$session = Registry::get('session');
			$session->create( $result['user_id'], $result['is_admin'] );
			include 'theme/landing.php.htm';
		} else {
			$errors[] = 'Invalid username or password';
			$login_errors = $errors;
			include 'theme/homepage.php.htm';
		}
	}
}

$db = Registry::get('db');
$session = Registry::get('session');

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'signup') {
		signup($db);
	} elseif ($_POST['action'] == 'login') {
		login($db);
	} else {
		include 'theme/homepage.php.htm';
	}
} else {
	if ( $session->logged_in() ) {
		include 'theme/landing.php.htm';
	} else {
		include 'theme/homepage.php.htm';
	}
}
?>
