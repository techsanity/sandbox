<?php
function get_header() {
    global $head;
    include __DIR__.'/../theme/header.php.htm';
}

function get_footer() {
    include __DIR__.'/../theme/footer.php.htm';
}

function logged_in() {
    $session = Registry::get('session');
    return $session->logged_in();
}

function is_admin() {
    $session = Registry::get('session');
    return $session->is_admin();
}