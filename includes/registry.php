<?php
class Registry {
    static $services = [];

    static function get( $id ) {
        return self::$services[ $id ];
    }
    static function register( $id, $object ) {
        self::$services[ $id ] = $object;
    }
}