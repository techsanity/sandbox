<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

include 'db.php';
include 'session.php';
include 'registry.php';
include 'template-functions.php';

// Page that is shown, when the sandbox is initialized
function on_intro($cookie_name, $sid) {
    include 'theme/sandbox-intro.php.htm';
    die();
}

// Function to initialize the database, after a cookie has been set
function on_setup($db) {
    $sql = file_get_contents('includes/schema.sql');
	$db->exec( $sql );
}

function init() {
    //$db_path, $on_intro, $on_setup, $cookie_name, $cookie_options 
    $db_path = realpath(__DIR__.'/../store');
    $db = new IsolatedDB($db_path, 'on_intro', 'on_setup', 'SANDBOX_ID', ['httponly' => true]);
    $db->init();
    Registry::register('db', $db);

    $session = new Session('facepunch_token');
    Registry::register('session', $session);
}