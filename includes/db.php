<?php
class IsolatedDB {
	var $conn;
	var $last;

	var $cookie_name;
	protected $cookie_options;
	var $sid;

	protected $db_path;
	protected $on_intro;
	protected $on_setup;


	function __construct( $db_path, $on_intro, $on_setup, $cookie_name, $cookie_options ) {
		$this->db_path = $db_path;
		$this->on_intro = $on_intro;
		$this->on_setup = $on_setup;
		$this->cookie_name = $cookie_name;
		$this->cookie_options = $cookie_options;

		if (!empty($_COOKIE[ $cookie_name ])) {
			$sid = $_COOKIE[ $cookie_name ];

			if (strlen($sid) == 16 &&
				preg_match('/^[a-zA-Z0-9_]+$/', $sid) == 1) {
				$this->sid = $sid;
				return;
			}
		}
		$this->sid = false;
	}

	static function gen_sid() {
		$sid = base64_encode(random_bytes(12));
		$sid = str_replace('/', '_', $sid);
		$sid = str_replace('+', '0', $sid);
		return $sid;
	}

	function cookie_setup( $name, $options, $on_intro = null ) {
		$this->sid = self::gen_sid();
		$this->cookie_options = $options;

		// Perhaps use setrawcookie() instead, to support SameSite in earlier PHP versions?
		if ( version_compare(PHP_VERSION, '7.3.0') >= 0 ) {
			setcookie( $this->cookie_name, $this->sid, $options );
		} else {
			$expires  = (empty($options['expires']))  ? 0     : $options['expires'];
			$path     = (empty($options['path']))     ? ''    : $options['path'];
			$domain   = (empty($options['domain']))   ? ''    : $options['domain'];
			@$secure   = (isset($options['secure']))   ? false : $options['secure'];
			@$httponly = (isset($options['httponly'])) ? false : $options['httponly'];

			setcookie( $this->cookie_name, $this->sid, $expires, $path, $domain, $secure, $httponly );
		}

		if ( $on_intro != null ) {
			call_user_func( $on_intro, $this->cookie_name, $this->sid );
		}
	}

	function init() {
		if ( $this->sid ) {
			// Cookie is set
			$file = $this->db_path.'/'.$this->sid.'.sqlite';
			
			if (!file_exists( $file )) {
				$needs_initialization = true;
			} else {
				$needs_initialization = false;
			}

			$this->conn = new \PDO( 'sqlite:'.$file );
			$this->restrictMulti( false );

			if ( $needs_initialization ) {
				// Setup DB only after we know they actually can retain a cookie
				//$on_setup( $this );
				call_user_func( $this->on_setup, $this );
			}
		} else {
			// Cookie not set
			$this->cookie_setup( $this->cookie_name, $this->cookie_options, $this->on_intro );
		}
	}

	function restrictMulti($bool) {
		return $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, $bool); // Turns on multi-query execution
	}

	function exec($sql, $die_on_err = true) {
		$this->last = $sql;
		$result = $this->conn->exec($sql);

		if ($die_on_err) {
			$this->dieIfErr();
		}
		return $result;
	}

	function query($sql, $die_on_err = true) {
		$this->last = $sql;
		$result = $this->conn->query($sql);

		if ($die_on_err) {
			$this->dieIfErr();
		}
		return $result;
	}

	function err() {
		$error = $this->conn->errorInfo();
		if (!empty($error[2])) {
			return $error[2];
		}
	}

	function showAll($table) {
		$rs = $this->q('SELECT * FROM '.$table);
		if ($rs == false) {
			echo "<h3>".$table." doesn't exist</h3>";
			return;
		}
		$rs->setFetchMode(PDO::FETCH_ASSOC);
		$columns = [];
		echo '<h3>'.$table.'</h3>';
		echo '<table class="table_'.$table.'"><thead><tr>';
		foreach($rs as $result) {
			if (!count($columns)) {
				$columns = array_keys($result);
				foreach($columns as $col) {
					echo "<td>".$col."</td>";
				}
				echo "</thead><tbody>";
			}
			echo '<tr>';
			foreach($columns as $col) {
				echo '<td>'.htmlspecialchars($result[$col]).'</td>';
			}
			echo '</tr>';
		}
		echo '</tbody></table>';
	}

	function dieIfErr() {
		$error_msg = $this->err();
		if ($error_msg) {
			$error_sql = $this->last;
			include __DIR__.'/../theme/error.php.htm';
			die();
		}
	}

	function drop() {
		$this->conn = null;
		$file = $this->db_path.'/'.$this->sid.'.sqlite';
		return unlink( $file );
	}
}
