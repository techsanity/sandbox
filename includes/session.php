<?php
class Session {
    var $cookie_name;
	var $uid = 0;
	var $admin = 0;

    var $logged_in = false;
    
    function __construct( $cookie_name ) {
        $this->cookie_name = $cookie_name;
		if (isset($_COOKIE[ $cookie_name ])) {
			$token = $_COOKIE[ $cookie_name ];

			$db = Registry::get('db');

			$sql = "SELECT user_id, level FROM sessions WHERE token = '$token'";
			$rs = $db->query( $sql );
			
			$result = $rs->fetch();
			if ( $result != false ) {
				$this->uid = $result['user_id'];
				$this->admin = $result['level'];
				$this->logged_in = true;
			} else {
				setcookie( $cookie_name, 'DE-YEET', 2);
			}
		}
    }
    
	function create($user_id, $is_admin) {
		$token = md5(time()); // This seems random enough, riiight?
		$sql = "INSERT INTO sessions (user_id, level, token) VALUES($user_id, $is_admin, '$token')";

		$db = Registry::get('db');
		$db->exec( $sql );

		setcookie( $this->cookie_name, $token, time()+2592000 );
		$this->logged_in = true;
		return true;
	}

	function expire() {
		if (isset($_COOKIE[ $this->cookie_name ])) {
			$token = $_COOKIE[ $this->cookie_name ];
			$sql = "DELETE FROM sessions WHERE token = '$token'";

			$db = Registry::get('db');
			$db->exec( $sql );
			$this->logged_in = false;

			setcookie( $this->cookie_name, 'DE-YEET', 2);
			return true;
		}
	}

	function logged_in() {
		return $this->logged_in;
	}

	function is_admin() {
		return $this->admin;
	}
}
