/*PRAGMA locking_mode = EXCLUSIVE;*/
PRAGMA synchronous = 3;

CREATE TABLE users(
	user_id INTEGER PRIMARY KEY AUTOINCREMENT,
  username VARCHAR(20),
	name VARCHAR(40),
  password VARCHAR(16),
	gender TEXT,
  is_admin TINYINT,
	is_hidden INT,
  last_login DATETIME,
  registered_on DATETIME,
  is_online INT
);

CREATE TABLE status(
  status_id INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id INT,
  user_to_id INT,
  user_action TEXT,
  is_hidden INT,
  posted_on DATETIME
);

CREATE TABLE wall(
  wall_user_id INT,
  poster_user_id INT,
  message TEXT,
  posted_on DATETIME
);

CREATE TABLE privmsg(
  from_user_id INT,
  to_user_id INT,
  message TEXT,
  posted_on DATETIME
);

CREATE TABLE photos(
  user_id INT,
  filename TEXT,
  posted_on DATETIME
);

CREATE TABLE sessions(
  token TEXT,
  user_id INT,
  level INT,
  started_on DATETIME
);

INSERT INTO users (user_id, username, name, password, gender, is_admin)
VALUES(1, 'admin', 'Zuck Cuckerberg', 'drowssap', 'Male', 1);